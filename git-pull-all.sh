#!/usr/bin/env bash

cd ../app-debt-process
git pull & git pull --tags

cd ../app-deployment
git pull; git pull --tags

cd ../app-link-process
git pull & git pull --tags

cd ../blauwe-knop
git pull & git pull --tags

cd ../debt-register
git pull & git pull --tags

cd ../debt-request-process
git pull & git pull --tags

cd ../debt-request-register
git pull & git pull --tags

cd ../deployment
git pull & git pull --tags

cd ../headless
git pull & git pull --tags

cd ../link-register
git pull & git pull --tags

cd ../login-page
git pull & git pull --tags

cd ../mobile-app
git pull & git pull --tags

cd ../scheme
git pull & git pull --tags

cd ../session-process
git pull & git pull --tags

cd ../session-register
git pull & git pull --tags

cd ../website
git pull & git pull --tags