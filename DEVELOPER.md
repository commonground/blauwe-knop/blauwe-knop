# Getting started

[[_TOC_]]

## Installation

Prerequisites:

- [Git](https://git-scm.com/)
- [Golang](https://golang.org/doc/install)
- [Modd](https://github.com/cortesi/modd)
- [Redis](https://redis.io)
- [GoMock](https://github.com/golang/mock)
- [Flutter](https://flutter.dev/docs/get-started/install)

### Backend

```sh
cd ../app-debt-process && go mod download
cd ../app-link-process && go mod download
cd ../debt-request-process && go mod download
cd ../debt-request-register && go mod download
cd ../link-register && go mod download
cd ../login-page && go mod download
cd ../session-process && go mod download
cd ../session-register && go mod download
```

## Run

### Running Redis

The backend uses a Redis database for data storage. You can launch it using Docker Compose.

```sh
docker-compose -f docker-compose.dev.yaml up -d
```

> The `-d` flag makes it run in the background. To stop the services simply type: `docker-compose stop`

### Backend

```sh
modd
```

> Note: When you use Virtual Android Device, you need to run:

```sh
cd ../blauwe-knop && modd -f modd-android.conf
```

### Mobile app

#### IOS

Run and debug mobile-app `local ios` from Visual Code Studio. (see launch.json)

Or via the terminal

```sh
cd ../mobile-app && flutter run -t lib/main_local.dart
```

#### Android

Run and debug mobile-app `local android` from Visual Code Studio. (see launch.json)

Or via the terminal

```sh
cd ../mobile-app && flutter run -t lib/main_local_android.dart
```

## Test

### Backend

Regenerate gomock files

```sh
cd ../app-debt-process && sh regenerate-gomock-files.sh
cd ../app-link-process && sh regenerate-gomock-files.sh
cd ../debt-request-process && sh regenerate-gomock-files.sh
cd ../debt-request-register && sh regenerate-gomock-files.sh
cd ../link-register && sh regenerate-gomock-files.sh
cd ../login-page && sh regenerate-gomock-files.sh
cd ../session-process && sh regenerate-gomock-files.sh
cd ../session-register && sh regenerate-gomock-files.sh
```

Run tests

```sh
cd ../app-debt-process && go test
cd ../app-link-process && go test
cd ../debt-request-process && go test
cd ../debt-request-register && go test
cd ../link-register && go test
cd ../login-page && go test
cd ../session-process && go test
cd ../session-register && go test
```

### Mobile app

```sh
cd ../mobile-app && flutter test
```
